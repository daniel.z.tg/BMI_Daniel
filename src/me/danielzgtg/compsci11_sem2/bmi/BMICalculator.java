package me.danielzgtg.compsci11_sem2.bmi;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Calculate someone's body mass index to indicate whether
 * or not that person is at a healthy body weight.
 *
 * @author  Ms Cianci, Daniel Tang
 * @since   Monday, September 15, 2008; 4 March 2017
 */
public class BMICalculator
{
	// constants used to represent the posible user's choices
	private static final int IMPERIAL_CHOICE = 1;
	private static final int METRIC_CHOICE = 2;	

	// create a Scanner object so that input can be obtained 
	// from within any method
	private static Scanner console = new Scanner(System.in);


	/** 
	 * Call on appropriate methods to get input from the user so that the
	 * corresponding BMI can be calculated and displayed
	 */
	public static void main(String[] args) 
	{			
		// display title
		System.out.println("\nHOW HEALTHY ARE YOU?");

		int choice = getChoice();

		int weight = getWeight(choice);
		double height = getHeight(choice);

		int bmi = getBMI(weight, height, choice);

		System.out.println("\n\nThe corresponding BMI is " + bmi + ".");

		displayHealthMessage(bmi);

	} // end main


	/**
	 * Ask the user to enter one for imperial and two for metric.
	 * Continue to ask the user to enter one or two if they enter 	 
	 * any other numeric value.
	 */
	public static int getChoice() 
	{
		while (true) { // Keep trying until good
			try {
				System.out.print("Use the imperial or metric system? (1 = imperial, 2 = metric): "); // Prompt for unit system
				final int result = console.nextInt(); // Obtain selection

				// Must be a 1 or 2 to be valid
				if (result == 1 || result == 2) {
					return result;
				}

				// It isn't right
				throw new InputMismatchException();
			} catch (final InputMismatchException ime) {
				// Something went wrong, try again
				System.out.println("Invalid selection, please try again.");
			} finally {
				// Clean up the console
				console.nextLine();
			}
		}
	}


	/**
	 * Get the user's weight (from the console) in either pounds or kgs and return the input.
	 * @param c Represents either imperial or metric units
	 * @return  The weight entered by the user (either in pounds or kilograms)
	 * NOTE: THIS METHOD HAS BEEN COMPLETED FOR YOU! JUST READ AND UNDERSTAND IT...
	 */
	public static int getWeight(int c) 
	{
		int result = -1;

		while (true) { // Keep trying until good
			try {
				if(c == IMPERIAL_CHOICE)
				{
					// get weight in pounds
					System.out.print("\nEnter weight in pounds: \t\t");
					String temp = console.next();
					result = Integer.parseInt(temp);
				}

				else if(c == METRIC_CHOICE)
				{
					// get weight in kilograms
					System.out.print("\nEnter weight in kilograms: \t\t\t\t");
					String temp = console.next();
					result = Integer.parseInt(temp);
				}

				if (result < 1) { // Weight cannot be zero or negative
					throw new IllegalArgumentException();
				}

				return result;
			} catch (final Exception e) { // Complain
				System.out.println("Invalid height, please try again.");
			} finally { // Flush buffer
				console.nextLine();
			}
		}
	} // end getWeight method


	/**
	 * Get the user's height (from the console) in either feet and inches or in meters and 
	 * return the height (in either inches or meters)
	 * @param c The user's choice, (representing either imperial or metric)
	 * @return The height entered by the user (either in inches or meters)
	 */
	public static double getHeight(int c)
	{
		while (true) { // Keep trying until good
			if(c == IMPERIAL_CHOICE) {
				try {
					System.out.print("What is you height in feet and inches? (123`123\"): "); // Prompt for height imperial
					String parseStr = console.nextLine().replaceAll("\\s+", ""); // Obtain String to parse

					// Must be atleast as long as the String "2'"
					if (parseStr.length() < 2) throw new IllegalArgumentException();

					double result = 0;
					boolean foundDecimalPoint = false;
					boolean feetParsed = false;
					int endIndex = 0;

					while (true) { // Iterate for both parts
						OUTERLOOP:
						while (true) { // Loop for section parsing
							if (endIndex >= parseStr.length()) {
								// Something is wrong if we reach the end like this
								throw new IllegalArgumentException();
							}
							char charr = parseStr.charAt(endIndex); // Get the current char

							switch (charr) {
							case '.':
								if (foundDecimalPoint) {
									// Complain if a decimal is invalid with two points
									throw new IllegalArgumentException();
								}

								// Make note of the point
								foundDecimalPoint = true;
								break;
							case '\'':
								// Feet must be first, and only once
								if (feetParsed) throw new IllegalArgumentException();

								// Make note that we are parsing feet now
								feetParsed = true;
								break OUTERLOOP;
							case '"':
								// Make note that we are parsing inches now
								feetParsed = false;
								break OUTERLOOP;
							default:
								// The only valid case left over is digits in the actual number
								if (!Character.isDigit(charr)) {
									throw new IllegalArgumentException();
								}
							}

							endIndex++; // Go to next 
						}

						if (feetParsed) { // If we are parsing feet
							// 12 Inches <==> 1 Foot
							// Add the section of feet to the result
							result += 12 * Double.parseDouble(parseStr.substring(0, endIndex));
							parseStr = parseStr.substring(endIndex + 1); // Move on from the number and the prime

							// Reset stuff
							endIndex = 0;
							foundDecimalPoint = false;

							if (parseStr.length() == 0) break; // Break if we only have feet
						} else { // If we are parsing inches
							// Add the section of inches to the result
							result += Double.parseDouble(parseStr.substring(0, endIndex));

							// Complain if there is stuff after inches--where there shouldn't be
							if (parseStr.length() != endIndex + 1) throw new IllegalArgumentException();
							break;
						}
					}

					return result;
				} catch (final IllegalArgumentException iae) {
					// Something wen't wrong, complain
					System.out.println("Invalid height, please try again.");
				}
			} else if(c == METRIC_CHOICE) {
				try {
					System.out.print("What is you height in metres? : "); // Prompt for height
					return console.nextDouble(); // Just return the height
				} catch (final InputMismatchException ime) {
					// Something went wrong, try again
					System.out.println("Invalid height, please try again.");
				} finally {
					// Clean up console
					console.nextLine();
				}
			} else {
				assert false;
			}
		}

	} // end getheight method


	/**
	 * @param w  The weight (in pounds or kilograms)
	 * @param h  The height (in inches or meters)
	 * @param c  The user's choice, (representing either imperial or metric)
	 * @return   The corresponding BMI
	 * NOTE: THIS METHOD HAS BEEN COMPLETED FOR YOU. YOU NEED TO READ AND UNDERSTAND IT.
	 */
	public static int getBMI(int w, double h, int c)
	{
		// if the user chose imperial then bmi = 703 x weight in pounds / height in inches squared
		if(c == IMPERIAL_CHOICE)
			return (int)(w * 703 / (h * h));

		// if the user chose metric then bmi = weight in kilograms / height in meters squared
		else if(c == METRIC_CHOICE)
			return (int)(w / (h * h));

		return -1;			// this should "never" be reached but the method always needs to return something
	} // end getBMI method


	/**
	 * Display a message describing a person's health with the given BMI
	 * @param bmi Someone's body mass index
	 * NOTE: THIS METHOD HAS BEEN COMPLETED FOR YOU. YOU NEED TO READ AND UNDERSTAND IT.
	 */
	public static void displayHealthMessage(int bmi)
	{
		// display a message describing if this is a healthy weight
		if(bmi < 18.5)
			System.out.println("This person is underweight.\n\n");
		else if(bmi < 24.9)
			System.out.println("This person is at a normal weight.\n\n");
		else if(bmi < 29.9)
			System.out.println("This person is overweight.\n\n");
		else
			System.out.println("This person is obese.\n\n");

	} // end displayHealthMessage method		

} // end class